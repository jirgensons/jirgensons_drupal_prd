<?php
/**
 * @file
 * jirgensons_drupal_prd3.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function jirgensons_drupal_prd3_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_frontpage';
  $strongarm->value = 'sakumlapa';
  $export['site_frontpage'] = $strongarm;

  return $export;
}
