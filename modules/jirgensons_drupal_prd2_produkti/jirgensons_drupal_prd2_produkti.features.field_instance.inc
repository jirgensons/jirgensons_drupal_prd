<?php
/**
 * @file
 * jirgensons_drupal_prd2_produkti.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function jirgensons_drupal_prd2_produkti_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-produkts-field_att_li'
  $field_instances['node-produkts-field_att_li'] = array(
    'bundle' => 'produkts',
    'deleted' => 0,
    'description' => 'Max attēlu skaits ir ierobežots līdz 5 attēliem',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_att_li',
    'label' => 'Attēli',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '5 MB',
      'max_resolution' => '600x600',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-produkts-field_cena'
  $field_instances['node-produkts-field_cena'] = array(
    'bundle' => 'produkts',
    'default_value' => array(
      0 => array(
        'value' => 0.01,
      ),
    ),
    'deleted' => 0,
    'description' => 'Cena eiro valūtā',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 2,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_decimal',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_cena',
    'label' => 'Cena',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => 0.01,
      'prefix' => '€',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-produkts-field_l_dz_gie_produkti'
  $field_instances['node-produkts-field_l_dz_gie_produkti'] = array(
    'bundle' => 'produkts',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Saites uz līdzīgiem produktiem (max 5 saites)',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_l_dz_gie_produkti',
    'label' => 'Līdzīgie produkti',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'optional',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => 'Saite',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-produkts-field_logo'
  $field_instances['node-produkts-field_logo'] = array(
    'bundle' => 'produkts',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_logo',
    'label' => 'Logo',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '3 MB',
      'max_resolution' => '150x150',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-produkts-field_noliktavas_st_voklis'
  $field_instances['node-produkts-field_noliktavas_st_voklis'] = array(
    'bundle' => 'produkts',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Noliktavas stāvoklis - ir, nav, jānoskaidro vai ir',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_noliktavas_st_voklis',
    'label' => 'Noliktavas stāvoklis',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-produkts-field_produkta_raksturojums'
  $field_instances['node-produkts-field_produkta_raksturojums'] = array(
    'bundle' => 'produkts',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Pievienojiet īsus produkta raksturojošus vārdus (līdz 255 simboliem)',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_produkta_raksturojums',
    'label' => 'Produkta raksturojums',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-produkts-field_ra_ot_js'
  $field_instances['node-produkts-field_ra_ot_js'] = array(
    'bundle' => 'produkts',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Pievieno ražotāju',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ra_ot_js',
    'label' => 'Ražotājs',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-produkts-field_tags'
  $field_instances['node-produkts-field_tags'] = array(
    'bundle' => 'produkts',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Produktu kategorija',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_tags',
    'label' => 'Kategorija',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-produkts-field_uid'
  $field_instances['node-produkts-field_uid'] = array(
    'bundle' => 'produkts',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Produkta unikālais identifikators noliktavā',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_uid',
    'label' => 'UID',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'taxonomy_term-produkti-field_att_li'
  $field_instances['taxonomy_term-produkti-field_att_li'] = array(
    'bundle' => 'produkti',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_att_li',
    'label' => 'Attēli',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '3 MB',
      'max_resolution' => '300x300',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'taxonomy_term-produkti-field_cena'
  $field_instances['taxonomy_term-produkti-field_cena'] = array(
    'bundle' => 'produkti',
    'default_value' => array(
      0 => array(
        'value' => 0.01,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 2,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_decimal',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_cena',
    'label' => 'Cena',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => 0.01,
      'prefix' => '€',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-produkti-field_produkta_raksturojums'
  $field_instances['taxonomy_term-produkti-field_produkta_raksturojums'] = array(
    'bundle' => 'produkti',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_produkta_raksturojums',
    'label' => 'Produkta raksturojums',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 6,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Attēli');
  t('Cena');
  t('Cena eiro valūtā');
  t('Kategorija');
  t('Logo');
  t('Līdzīgie produkti');
  t('Max attēlu skaits ir ierobežots līdz 5 attēliem');
  t('Noliktavas stāvoklis');
  t('Noliktavas stāvoklis - ir, nav, jānoskaidro vai ir');
  t('Pievieno ražotāju');
  t('Pievienojiet īsus produkta raksturojošus vārdus (līdz 255 simboliem)');
  t('Produkta raksturojums');
  t('Produkta unikālais identifikators noliktavā');
  t('Produktu kategorija');
  t('Ražotājs');
  t('Saites uz līdzīgiem produktiem (max 5 saites)');
  t('UID');

  return $field_instances;
}
