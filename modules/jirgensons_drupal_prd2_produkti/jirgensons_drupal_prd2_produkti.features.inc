<?php
/**
 * @file
 * jirgensons_drupal_prd2_produkti.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function jirgensons_drupal_prd2_produkti_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function jirgensons_drupal_prd2_produkti_node_info() {
  $items = array(
    'produkts' => array(
      'name' => t('Produkts'),
      'base' => 'node_content',
      'description' => t('Satura tips produktiem'),
      'has_title' => '1',
      'title_label' => t('Produkta nosaukums'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
